<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::group(['prefix' => 'admin'], function () {
    // Route::get('/', function () {
    //     return view('admin.base');
    // });

    // Route::get('login', function () {
    //     return view('admin.login');
    // });

    // Route::auth();

    // Route::resource('contact', 'admin\ContactController');


    Route::group(['middleware' => 'web'], function () {
        // Route::auth();
        // var_dump($this->post);exit;
        // Authentication Routes...
        $this->get('login', 'Auth\AuthController@showLoginForm');
        $this->post('login', 'Auth\AuthController@login');
        $this->get('logout', 'Auth\AuthController@logout');
        // Registration routes...
        $this->get('register', 'Auth\AuthController@showRegisterForm');
        $this->post('register', 'Auth\AuthController@register');
        // Registration..

        // Route::get('/home', 'HomeController@index');

        // Route::get('/', function () {
        //     return view('admin.base');
        // });

        // Route::get('login', function () {
        //     return view('admin.login');
        // });

        // Route::resource('contact', 'admin\ContactController');

        // Route::get('/', function () {
        //     return view('admin.base');
        // });

        Route::group(['middleware' => 'auth'], function () {


            // Route::get('/', 'HomeController@index');

            Route::get('/', function () {

                // $user = Auth::user();

                return view('admin.base');
            });

            // Route::get('login', function () {
            //     return view('admin.login');
            // });

            Route::resource('contact', 'Admin\ContactController');
            Route::resource('profile', 'Admin\ProfileController');

            Route::resource('news',    'Admin\NewsController');
            Route::post('activate/{id}',    'Admin\Ajaxcontroller@setActive');
            Route::post('delete/{id}',    'Admin\Ajaxcontroller@deletItem');

            Route::resource('cities', 'Admin\CitiesController');
            Route::post('post/nestable', 'Admin\PostController@nestable');

        });

    });



});





/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

// Route::group(['middleware' => ['web']], function () {
//     //
// });

// Route::group(['middleware' => 'web'], function () {
//     Route::auth();

//     Route::get('/home', 'HomeController@index');
// });
