<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Artisan;

class modelGenerator extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate:model {modelName}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command generate specifying model';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {

        parent::__construct();
    }

    public function generateModel($modelName)
    {   
        echo(Artisan::call('make:model',['name' => $modelName]));
        // if(Artisan::call('make:model',['name' => $modelName])){
        //     $this->fields();
        // }else{
        //     $this->error('Something went wrong!');
        // }
        
        
    }

    public function fields()
    {
        $field = $this->ask('field');
        $fieldType = $this->ask('field type');
        $fields[$field] = $fieldType;
        var_dump($fields);exit;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->generateModel($this->argument('modelName'));
    }
}
