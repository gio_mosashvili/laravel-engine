@extends('admin.base')
@section('page-styles')
  <link rel="stylesheet" href="plugins/bootstrap-markdown/css/bootstrap-markdown.min.css">
@endsection

@section('content')

<section class="panel">
  <header class="panel-heading">Article Edit</header>
  <div class="panel-body">
    <div class="form-group">
      <div class="col-sm-10">
          {{ Form::open(array('url' => '/admin/news/'.$article->id, 'method' => 'PUT','files' => true)) }}
                {{ Form::text('title',$article->title,array( 'class' => 'form-control mb25','placeholder' => 'Title' ) )}}
                {{ Form::textarea('text',$article->text,array(  'data-iconlibrary' =>'fa', 'data-provide' => 'markdown','rows' => '10', 'placeholder' => 'Text') )}}<br>
                {{ Form::file('image')}}<br>
                <!-- <div style=";position: relative; display: inline-block;">
                  <button style="position: absolute;top: -15px;right: -10px;margin-right: 0;" class="delete-image btn btn-default btn-sm mr5 active">
                  </button>
                  <img style="height:200px" src="<?= Config::get('app.url') ?>/uploads/article/<?=$article->image ?>"><br><br>
                </div> -->
                {{ Form::text('published_at',$article->published_at,array( 'data-date-format'=>'yyyy-mm-dd', 'class' => 'form-control datepicker' ) )}}
                {{ Form::submit('Save',array('class'=>'btn btn-success mt25'))}}
          {{ Form::close() }}
      </div>
    </div>
  </div>
</section>
@endsection
@section('page-scripts')
  <script src="plugins/wysiwyg/bootstrap-wysiwyg.js"></script>
  <script src="plugins/bootstrap-markdown/js/bootstrap-markdown.js"></script>

  <!-- page level scripts -->
  <script src="plugins/datepicker/bootstrap-datepicker.js"></script>
<!-- /page level scripts -->
@endsection
