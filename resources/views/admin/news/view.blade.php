@extends('admin.base')
@section('page-styles')
  <link rel="stylesheet" href="plugins/chosen/chosen.min.css">
  <link rel="stylesheet" href="plugins/datatables/jquery.dataTables.css">
@endsection

@section('content')
  <div class="clear"></div>
  <section class="panel panel-default">
      <header class="panel-heading">All Article</header>
      <input class="url-helper" type="hidden" value="<?= url('/admin/news/create') ?>">
      <div class="clear"></div>
      <div class="panel-body">
          <div class="table-responsive no-border">

                <table class="table table-bordered table-striped mg-t datatable" data-tablename="articles">
                  <thead>
                      <tr>
                        <th>Title</th>
                        <th>Text</th>
                        <th>Published at</th>
                        <th>Active</th>
                        <th>Action</th>
                      </tr>
                  </thead>
                  <tbody>
                    @foreach($articles as $article)
                      <tr>
                          <td>{{ $article->title }}</td>
                          <td>{{ $article->text }}</td>
                          <td>{{ $article->published_at }}</td>
                          <td>
                            <div class="mr15">
                              {{ Form::checkbox(null,null,$article->active ? true : false, array( 'data-token' => csrf_token(),'class' => 'js-switch-green','data-id'=> $article->id ) )}}
                            </div>
                          </td>
                          <td>
                            <a href="<?= url('/admin/news/'.$article->id.'/edit') ?>" class="btn btn-default btn-sm mr5"><i class="fa fa-edit"></i></a>
                            <a href="javascript:;" class="btn btn-danger btn-sm mr5 delete" data-token="{{ csrf_token() }}" data-id="<?= $article->id ?>"><i class="fa fa-remove"></i></a>
                          </td>
                      </tr>
                    @endforeach
                  </tbody>
              </table>
          </div>
      </div>
  </section>
@endsection
@section('page-scripts')
<script src="plugins/datatables/jquery.dataTables.js"></script>

<script src="js/bootstrap-datatables.js"></script>
<script src="js/datatables.js"></script>

<script src="js/listAction.js"></script>


<script src="plugins/chosen/chosen.jquery.min.js"></script>
<script src="plugins/datatables/jquery.dataTables.js"></script>

<script src="js/form-custom.js"></script>

<script src="plugins/switchery/switchery.js"></script>


@endsection
