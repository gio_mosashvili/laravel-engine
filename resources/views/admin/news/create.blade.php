@extends('admin.base')
@section('page-styles')
  <link rel="stylesheet" href="plugins/bootstrap-markdown/css/bootstrap-markdown.min.css">
@endsection
<?php
// $carbonNow = Carbon\Carbon::now();
// $published_at = $carbonNow->format('Y-m-d');
//
?>
@section('content')

<section class="panel">
  <header class="panel-heading">Add Article</header>
  <div class="panel-body">
    <div class="form-group">
      <div class="col-sm-10">
        {{ Form::open(array('url' => '/admin/news/', 'method' => 'POST','files' => true)) }}
              {{ Form::text('title',null,array( 'class' => 'form-control mb25','placeholder' => 'Title' ) )}}
              {{ Form::textarea('text',null,array(  'data-iconlibrary' =>'fa', 'data-provide' => 'markdown','rows' => '10', 'placeholder' => 'Text') )}}<br>
              {{ Form::file('image',null,array( 'class' => 'fbtn btn-default btn-sm mr5' ) )}}<br>
              {{ Form::text('published_at',Carbon\Carbon::now(),array( 'data-date-format'=>'yyyy-mm-dd', 'class' => 'form-control datepicker' ) )}}
              {{ Form::submit('Finish',array('class'=>'btn btn-success mt25'))}}
        {{ Form::close() }}
      </div>
    </div>
  </div>
</section>
@endsection
@section('page-scripts')
<script src="plugins/wysiwyg/bootstrap-wysiwyg.js"></script>
<script src="plugins/bootstrap-markdown/js/bootstrap-markdown.js"></script>

<!-- page level scripts -->
<script src="plugins/datepicker/bootstrap-datepicker.js"></script>
<!-- /page level scripts -->
@endsection
