@extends('admin.base')

@section('content')
<section class="panel">
  <div class="row">
      <div class="col-lg-12">
          <section class="panel">
              <header class="panel-heading">Profile Information</header>
              <div class="panel-body">
                  {{ Form::model($user, array('url'=>url('/admin/profile'), 'class'=>'form-horizontal', 'role'=>'form')) }}
                      <div class="form-group">
                          <label class="col-sm-2 control-label">Username name</label>
                          <div class="col-sm-10">
                              {{ Form::text('username', null, array('class'=>'form-control', 'required')) }}
                          </div>
                      </div>
                      <div class="form-group">
                          <label class="col-sm-2 control-label">Email</label>
                          <div class="col-sm-10">
                              {{ Form::email('email', null, array('class'=>'form-control', 'required')) }}
                          </div>
                      </div>
                      <div class="form-group">
                          <label class="col-sm-2 control-label">Password</label>
                          <div class="col-sm-10">
                              {{ Form::password('password', array('class'=>'form-control', 'required', 'pattern' => '.{6,}', 'title'=>'6 characters minimum')) }}
                          </div>
                      </div>
                      <div class="form-group">
                          <div class="col-sm-12 text-right">
                              {{ Form::submit('Edit',  array('class'=>'btn btn-primary')) }}
                          </div>
                      </div>
                  {{ Form::close() }}
              </div>
          </section>
      </div>
  </div>
</section>
@endsection