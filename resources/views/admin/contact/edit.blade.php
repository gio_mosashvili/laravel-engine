@extends('admin.base')

@section('content')
  <div class="row">
      <div class="col-lg-12">
          <section class="panel">
              <header class="panel-heading">Contact Information</header>
              <div class="panel-body">
                  {{ Form::model($contact, array('method'=>'PUT', 'action'=>array('Admin\ContactController@update', 1), 'class'=>'form-horizontal', 'role'=>'form')) }}
                      <div class="form-group">
                          <label class="col-sm-2 control-label">Email</label>
                          <div class="col-sm-10">
                              {{ Form::text('email', null, array('class'=>'form-control')) }}
                          </div>
                      </div>
                      <div class="form-group">
                          <label class="col-sm-2 control-label">Tel</label>
                          <div class="col-sm-10">
                              {{ Form::text('tel', null, array('class'=>'form-control')) }}
                          </div>
                      </div>
                      <div class="form-group">
                          <label class="col-sm-2 control-label">Facebook</label>
                          <div class="col-sm-10">
                              {{ Form::text('fb', null, array('class'=>'form-control')) }}
                          </div>
                      </div>
                      <div class="form-group">
                          <div class="col-sm-12 text-right">
                            {{ Form::submit('Edit', array('class'=>'btn btn-primary')) }}
                          </div>
                      </div>
                  {{ Form::close() }}
              </div>
          </section>
      </div>
  </div>
@endsection