@extends('admin.base')

@section('content')
  <div class="row">
    <div class="col-lg-12">
      <header class="panel-heading">Add city</header>

      {{ Form::open(['action'=>'Admin\CitiesController@store', 'class'=>'form-horizontal', 'role'=>'form']) }}
      <div class="row">
        <div class="col-md-12">
          <div class="box-tab tabs-right">
            <ul class="nav nav-tabs">
              <?php foreach (Config::get('app.locales') as $key => $locale): ?>
                <li {!! $key == 0 ? 'class = "active"' : "" !!}><a href="#{{ $locale }}" data-toggle="tab">{{ $locale }}</a></li>
              <?php endforeach ?>
            </ul>
            <div class="tab-content">
              <?php foreach (Config::get('app.locales') as $key => $locale): ?>
                <div class="tab-pane fade {!! $key == 0 ? 'active in' : '' !!}" id="{{ $locale }}">
                  <div class="form-group">
                      <label class="col-sm-2 control-label">Title</label>
                      <div class="col-sm-10">
                        {{ Form::text('title['.$locale.']', null, array('class'=>'form-control')) }}
                      </div>
                  </div>            
                </div>
              <?php endforeach ?>
            </div>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-md-12">
          <div class="form-group">
            <div class="col-sm-12 text-right">
              {{ Form::submit('Add', array('class'=>'btn btn-primary')) }}
            </div>
          </div>
        </div>
      </div>
      {{ Form::close() }}

<?php /*              <div class="panel-body">
                  {{ Form::open(['action'=>'Admin\CitiesController@store', 'class'=>'form-horizontal', 'role'=>'form']) }}
                      <div class="form-group">
                          <label class="col-sm-2 control-label">Title</label>
                          <div class="col-sm-10">
                              {{ Form::text('title', null, array('class'=>'form-control')) }}
                          </div>
                      </div>
                      <div class="form-group">
                          <div class="col-sm-12 text-right">
                            {{ Form::submit('Add', array('class'=>'btn btn-primary')) }}
                          </div>
                      </div>
                  {{ Form::close() }}
              </div> */ ?>
    </div>
  </div>
@endsection