@extends('admin.base')

@section('content')
  <div class="row">
      <div class="col-lg-12">
        <div class="row">
          <div class="col-xs-6"><header class="panel-heading">List of cities</header></div>
          <div class="col-xs-6 text-right"><a href="{{ url('/admin/cities/create') }}" class="btn btn-primary">Add</a></div>
        </div>
        
        <div class="cf nestable-lists">
          <div class="dd" id="nestable" data-table="cities">
            <ol class="dd-list">
              @foreach($cities as $item)
              <li class="dd-item" data-id="{{ $item->id }}">
                <div class="dd-handle">{{ $item->title }}</div>
                <div style="position:absolute;right:4px;top:4px;">
                  <a href="<?= url('admin/cities/'.$item->id.'/edit') ?>" class="btn btn-default btn-outline"><i class="fa fa-edit"></i></a> 
                  {{ Form::open(array('method' => 'DELETE', 'action'=>['Admin\CitiesController@destroy', $item->id], 'style'=>'display:inline-block')) }}
                  {{ Form::button('<i class="fa fa-remove"></i>', array('type'=>'submit', 'class'=>'btn btn-default', 'onClick' => "return confirm('Are you sure you want to delete this item?');")) }}
                  {{ Form::close() }}
                </div>
              </li>
              @endforeach
            </ol>
          </div>
        </div>
      </div>
  </div>
@endsection

@section('page-scripts')
<script src="plugins/jquery.sortable.js"></script>
<script src="plugins/jquery.nestable.js"></script>
<script src="js/sortable.js"></script>
@stop