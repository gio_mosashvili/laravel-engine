$(function(){

  $('.dd').nestable({
    maxDepth: 1
  });

  var list = $('.dd');
  var table = list.data('table');

  list.on('change', function(){
    $.post( '/admin/post/nestable', {tablename: table, data: $('.dd').nestable('serialize')});
  });

});