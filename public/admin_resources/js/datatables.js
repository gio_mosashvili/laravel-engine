var demoDataTables = function () {

    var tableName = $('.datatable').data().tablename;
    function events() {
        $('.datatable').on("click",".switchery",function(e){
          var checkBox = $(this).parent().find('input');
          checkBox.attr("checked", !checkBox.attr("checked"));
          var postUrl =  SITE_URL+'/admin/activate/'+checkBox.data().id;
          var itemToken = checkBox.data().token;
          var active = checkBox.attr("checked") ? 1 : 0;

          $.ajax({
              url: postUrl,
              type: 'post',
              data: {_token :itemToken,active,tableName},
              error: function(){
                alert('Setactive or Deactive failure');
              }
          });

        })

        $('.datatable').on("click", "a.delete", function (e) {
            e.preventDefault();
            var postUrl =  SITE_URL+'/admin/delete/'+$(this).data().id;
            var itemToken = $(this).data().token;
            var nRow = $(this).parents('tr')[0];
            if (confirm('Are you sure ?')) {
              $.ajax({
                  url: postUrl,
                  type: 'post',
                  data: {_token :itemToken,tableName},
                  success: function(){
                    var item = oTable.fnDeleteRow(nRow);
                  },
                  error: function(){
                    alert('Item Delete Failure');
                  }
              });
            }
        });
    }

    function plugins() {
        oTable = $('.table-striped').dataTable({
            "sDom": "<'row'<'col-xs-6'l <'toolbar'>><'col-xs-6'f>r>t<'datatable-bottom'<'pull-left'i><'pull-right'p>>"
        });

        var addArticleHref = $('input.url-helper')[0].value;

        $(".toolbar").append("<a style='margin-left:15px;' href="+addArticleHref+" class='btn btn-success'>New item</a>");

        $('.chosen').chosen({
            width: "80px"
        });
    }



    return {
        init: function () {
            events();
            plugins();

            $('.chosen').chosen({
                width: "80px"
            })
        }

    };
}();

$(function () {
    "use strict";
    demoDataTables.init();
});
